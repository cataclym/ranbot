﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using Discord.WebSocket;
using RanBot.Services;
using RanBot.Services.Impl;

namespace RanBot
{
    class RanBot
    {
        private DiscordSocketClient _client;
        private IBotCredentials _creds;
        private DbService _db;

        public async Task Ran()
        {
            _client = new DiscordSocketClient();
            _creds = new BotCredentials();
            _db = new DbService(_creds);

            _db.Setup();

            await _client.LoginAsync(TokenType.Bot, _creds.Token);
            await _client.StartAsync();

            var services = RanServiceProvider();
            services.GetRequiredService<RanLogger>();
            services.GetRequiredService<CommandHandler>();

            await Task.Delay(-1);

        }
        public IServiceProvider RanServiceProvider() => new ServiceCollection()
            .AddSingleton(_client)
            .AddSingleton(_db)
            .AddSingleton<CommandService>()
            .AddSingleton<CommandHandler>()
            .AddLogging()
            .AddSingleton<RanLogger>()
            .BuildServiceProvider();
    }
}