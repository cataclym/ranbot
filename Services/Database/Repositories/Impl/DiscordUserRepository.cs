using RanBot.Services.Database.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Discord;
using System.Collections.Generic;
using System;

namespace RanBot.Services.Database.Repositories.Impl
{
    public class DiscordUserRepository : Repository<DiscordUser>, IDiscordUserRepository
    {
        public DiscordUserRepository(DbContext context) : base(context)
        {
        }

        public void EnsureCreated(ulong userId, string username, string discrim)
        {
            _context.Database.ExecuteSqlInterpolated($@"
UPDATE OR IGNORE DiscordUser
SET Username={username},
    Discriminator={discrim},
WHERE UserId={userId};

INSERT OR IGNORE INTO DiscordUser (UserId, Username, Discriminator, AvatarId)
VALUES ({userId}, {username}, {discrim});
");
        }

        //temp is only used in updatecurrencystate, so that i don't overwrite real usernames/discrims with Unknown
        public DiscordUser GetOrCreate(ulong userId, string username, string discrim)
        {
            EnsureCreated(userId, username, discrim);
            return _set
                .First(u => u.UserId == userId);
        }

        public DiscordUser GetOrCreate(IUser original)
            => GetOrCreate(original.Id, original.Username, original.Discriminator);

        public int GetUserGlobalRank(ulong id)
        {
            return _set.AsQueryable()
                .Where(x => x.TotalCounted > (_set
                    .AsQueryable()
                    .Where(y => y.UserId == id)
                    .Select(y => y.TotalCounted)
                    .FirstOrDefault()))
                .Count() + 1;
        }

        public DiscordUser[] GetUsersCountLeaderboardFor(int page)
        {
            return _set.AsQueryable()
                .OrderByDescending(x => x.TotalCounted)
                .Skip(page * 9)
                .Take(9)
                .AsEnumerable()
                .ToArray();
        }
    }
}
