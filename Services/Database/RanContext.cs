using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using RanBot.Services.Database.Models;
using RanBot.Services.Impl;
using System;
using System.IO;
using System.Linq;

namespace RanBot.Services.Database
{
    public class RanContextFactory : IDesignTimeDbContextFactory<RanContext>
    {
        public RanContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<RanContext>();
            IBotCredentials creds = new BotCredentials();
            var builder = new SqliteConnectionStringBuilder(creds.Db.ConnectionString);
            builder.DataSource = Path.Combine(AppContext.BaseDirectory, builder.DataSource);
            optionsBuilder.UseSqlite(builder.ToString());
            var ctx = new RanContext(optionsBuilder.Options);
            ctx.Database.SetCommandTimeout(60);
            return ctx;
        }
    }

    public class RanContext : DbContext
    {
        public DbSet<BotConfig> BotConfig { get; set; }
        public DbSet<GuildConfig> GuildConfigs { get; set; }

        public RanContext(DbContextOptions<RanContext> options) : base(options)
        {
        }

        public void EnsureSeedData()
        {
            if (!BotConfig.Any())
            {
                var bc = new BotConfig();

                BotConfig.Add(bc);

                this.SaveChanges();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region GuildConfig

            var configEntity = modelBuilder.Entity<GuildConfig>();
            configEntity
                .HasIndex(c => c.GuildId)
                .IsUnique();

            #endregion

            #region BotConfig
            var botConfigEntity = modelBuilder.Entity<BotConfig>();

            botConfigEntity.Property(x => x.OkColor)
                .HasDefaultValue("009113");

            botConfigEntity.Property(x => x.ErrorColor)
                .HasDefaultValue("910000");
                
            botConfigEntity.Property(x=> x.DefaultLanguage)
                .HasDefaultValue("en-US");

            #endregion

            #region DiscordUser

            var du = modelBuilder.Entity<DiscordUser>();
            du.HasAlternateKey(w => w.UserId);

            #endregion

            #region CountingStats
            var cnts = modelBuilder.Entity<UserCountingStats>();
            cnts
                .HasIndex(x => new { x.UserId, x.GuildId })
                .IsUnique();

            cnts.HasIndex(x => x.UserId);
            cnts.HasIndex(x => x.GuildId);

            #endregion
        }
    }
}
