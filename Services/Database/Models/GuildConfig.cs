using Discord;
using System;
using System.Collections.Generic;

namespace RanBot.Services.Database.Models
{
    public class GuildConfig : DbEntity
    {
        public ulong GuildId { get; set; }

        public string Prefix { get; set; } = null;
        public ulong CountingChannelId { get; set; }

        public string OkColor { get; set; }

        public string ErrorColor { get; set; }

        public bool CountDown { get; set; } = false;

        public bool AllowMessages { get; set; } = false;

        public string DefaultLanguage { get; set; }

        //public CountingSettings CountingSettings { get; set; }
    }
}