using System;
using System.Threading.Tasks;
using RanBot.Services.Database.Repositories;

namespace RanBot.Services.Database
{
    public interface IUnitOfWork : IDisposable
    {
        RanContext _context { get; }

        IGuildConfigRepository GuildConfigs { get; }
        IBotConfigRepository BotConfig { get; }
        IDiscordUserRepository DiscordUsers { get; }
        IUserCountingStatsRepository UserCountingStats { get; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}