using System;
using System.Runtime.CompilerServices;
using System.Linq;
using Discord.Commands;
using Newtonsoft.Json;
using RanBot.Services.Impl;

namespace RanBot.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class RanCommandAttribute : CommandAttribute
    {
        private static CommandList _commandList = new CommandList("en-US");
        public RanCommandAttribute([CallerMemberName] string memberName="") : base(_commandList.LoadCommand(memberName.ToLowerInvariant()).Cmd)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public sealed class AliasesAttribute : AliasAttribute
    {
        private static CommandList _commandList = new CommandList("en-US");
        public AliasesAttribute([CallerMemberName] string memberName = "") : base(_commandList.LoadCommand(memberName.ToLowerInvariant()).Aliases.Split(' ').ToArray())
        {
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public sealed class DescriptionAttribute : SummaryAttribute
    {
        private static CommandList _commandList = new CommandList("en-US");
        public DescriptionAttribute([CallerMemberName] string memberName="") : base(_commandList.LoadCommand(memberName.ToLowerInvariant()).Desc)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public sealed class UseAttribute : RemarksAttribute
    {
        private static CommandList _commandList = new CommandList("en-US");
        public UseAttribute([CallerMemberName] string memberName="") : base(UseAttribute.GetUse(memberName))
        {
        }

        public static string GetUse(string memberName)
        {
            var use = _commandList.LoadCommand(memberName.ToLowerInvariant()).Use;
            return JsonConvert.SerializeObject(use);
        }
    }


}